data "terraform_remote_state" "base" {
  backend = "s3"
  config  = {
    bucket = var.base_state_bucket
    key    = "terraform/base"
    region = var.aws_region
  }
  //    role_arn     = "${var.arch_assumed_role}"
  //    session_name = "terraform-infra-base-${var.environment}"
  //    external_id  = "terraform"
}


locals {
  environments             = data.terraform_remote_state.base.outputs.environments
  tags_common_base         = data.terraform_remote_state.base.outputs.tags_common
  shared_tag               = data.terraform_remote_state.base.outputs.shared_tag
  infra_public_bucket      = data.terraform_remote_state.base.outputs.infra_public_bucket
  infra_prefix             = data.terraform_remote_state.base.outputs.infra_prefix
  arch_access_user         = data.terraform_remote_state.base.outputs.managed_access_user
  arch_access_key          = data.terraform_remote_state.base.outputs.managed_access_key
  dns_arch_main_zone       = data.terraform_remote_state.base.outputs.dns_zone
  dns_arch_main_name       = "${var.environment}.${data.terraform_remote_state.base.outputs.dns_name}"
  dns_arch_main_suffix     = local.dns_arch_main_name
  dns_arch_services_zone   = data.terraform_remote_state.base.outputs.dns_service_zone
  dns_arch_services_name   = "${var.environment}.${data.terraform_remote_state.base.outputs.dns_service_name}"
  dns_arch_services_suffix = local.dns_arch_services_name
  ecr_proxy                = data.terraform_remote_state.base.outputs.ecr_proxy
  ecr_proxy_account_id     = data.terraform_remote_state.base.outputs.ecr_proxy_account_id
  email_user               = data.terraform_remote_state.base.outputs.email_user
  email_key                = data.terraform_remote_state.base.outputs.email_key
  k8s_name                 = data.terraform_remote_state.base.outputs.k8s_name

  env = local.environments[var.environment]
}

