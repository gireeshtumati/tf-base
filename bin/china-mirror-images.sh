#!/usr/bin/env bash
set -ex

WORKDIR="$(cd "$(dirname "${BASH_SOURCE[0]}")" >/dev/null 2>&1 && pwd)/.."
REMOTE_REPO="951794384439.dkr.ecr.cn-northwest-1.amazonaws.com.cn/hilti-mirror"

readarray -t ROWS < "${WORKDIR}/data/china-mirror-list.txt"
for ROW in "${ROWS[@]}"; do
  IFS=' ' read -ra IMG <<< "${ROW}"

  IMAGE_ORIGIN="${IMG[0]}"
  IMAGE_TARGET="${REMOTE_REPO}:${IMG[1]}"
  echo "Pulling ${IMAGE_ORIGIN}..."
  docker pull "${IMAGE_ORIGIN}"
  echo "Pushing it to ${IMAGE_TARGET}..."
  docker tag "${IMAGE_ORIGIN}" "${IMAGE_TARGET}"
  docker push "${IMAGE_TARGET}"
done
