data "aws_caller_identity" "current" {
}

data "aws_partition" "current" {}

data "template_file" "gitlab-runner-init-docker" {
  count    = local.create-instances
  template = file("${path.module}/data/init-docker.sh")

  vars = {
    DOMAIN             = var.gitlab_domain
    REGION             = var.aws_region
    LOG_GROUP          = aws_cloudwatch_log_group.gitlab-runner.name
    DISK_INODES        = var.runner_disk_inodes
    ENV                = upper(var.environment)
    ENV_LOWER          = lower(var.environment)
    CACHE_BUCKET       = var.runner_cache_bucket
    CACHE_PREFIX       = var.runner_cache_prefix
    RUNNER_VERSION     = var.runner_version
    RUNNER_LIMIT       = var.runner_limit
    REGISTRATION_TOKEN = var.runner_registration_token
    TAGS               = join(",", var.runner_tags)
  }
}

data "template_file" "gitlab-runner-init-windows" {
  count    = local.create-instances
  template = file("${path.module}/data/init-windows.ps1")

  vars = {
    DOMAIN             = var.gitlab_domain
    REGION             = var.aws_region
    LOG_GROUP          = aws_cloudwatch_log_group.gitlab-runner.name
    ENV                = upper(var.environment)
    ENV_LOWER          = lower(var.environment)
    CACHE_BUCKET       = var.runner_cache_bucket
    CACHE_PREFIX       = var.runner_cache_prefix
    RUNNER_VERSION     = var.runner_version
    RUNNER_LIMIT       = var.runner_limit
    REGISTRATION_TOKEN = var.runner_registration_token
    TAGS               = join(",", var.runner_tags)
  }
}

resource "random_string" "windows-password" {
  count = var.runner_count

  length      = 12
  special     = false
  min_upper   = 1
  min_lower   = 1
  min_numeric = 1
}

data "null_data_source" "gitlab-runner-data" {
  count = var.runner_count

  inputs = {
    init-docker = <<EOF
#!/usr/bin/env bash
set -xe
export RUNNER_ID=${count.index + 1}
${data.template_file.gitlab-runner-init-docker[0].rendered}
EOF

    init-windows = <<EOF
<script>
net user admin ${element(random_string.windows-password.*.result, count.index)} /add /y
net localgroup administrators admin /add
net localgroup "Remote Desktop Users" "admin" /add
</script>
<powershell>
$RUNNER_ID=${count.index + 1}
${data.template_file.gitlab-runner-init-windows[0].rendered}
</powershell>
<persist>true</persist>
EOF

}
# TODO: add
# git config --system core.longpaths true
# Invoke-WebRequest -Uri 'https://go.microsoft.com/fwlink/?linkid=2088631' -Outfile 'ndp48-x86-x64-allos-enu.exe'
# Start-Process ".\ndp48-x86-x64-allos-enu.exe" -Wait -NoNewWindow -ArgumentList '/passive', '/norestart'
# Invoke-WebRequest -Uri 'https://go.microsoft.com/fwlink/?linkid=2088517' -Outfile 'ndp48-devpack-enu.exe'
# Start-Process ".\ndp48-devpack-enu.exe" -Wait -NoNewWindow -ArgumentList '/passive', '/norestart'
}

locals {
name = "${var.module_prefix}gitlab-runner${var.runner_type == "docker" ? "" : format("-%s", var.runner_type)}"
local-tags = {
Name = "${local.name}-${var.runner_type}"
}
tags = merge(var.tags, local.local-tags)
}

