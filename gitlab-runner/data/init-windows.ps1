$ErrorActionPreference="Stop"
[Net.ServicePointManager]::SecurityProtocol = [Net.SecurityProtocolType]::Tls12

$EC2SettingsFile="C:\Program Files\Amazon\Ec2ConfigService\Settings\Config.xml"
$xml = [xml](get-content $EC2SettingsFile)
$xmlElement = $xml.get_DocumentElement()
$xmlElementToModify = $xmlElement.Plugins
foreach ($element in $xmlElementToModify.Plugin)
{
    if ($element.name -eq "Ec2EventLog")
    {
        $element.State="Enabled"
    }
}
$xml.Save($EC2SettingsFile)

# Check and install latest PowerShell
write-host("Poweshell version: " + $PSVersionTable.PSVersion.Major + "." + $PSVersionTable.PSVersion.Minor)
if( -not ($PSVersionTable.PSVersion.Major -ge 5 -and $PSVersionTable.PSVersion.Minor -ge 1) )
{
    write-host("Installing powershell v5.1")
    $Source = 'https://go.microsoft.com/fwlink/?linkid=839516'
    $destination = Join-Path $env:TEMP -ChildPath pw-install.msu
    $destinationTarget = Join-Path $env:TEMP -ChildPath pw-install

    Invoke-WebRequest -Uri $Source -OutFile $destination
    Start-Process -FilePath wusa.exe -Wait -ArgumentList "$destination /extract:$destinationTarget"
    Start-Process -FilePath dism.exe -Wait -ArgumentList "/online /add-package /Quiet /PackagePath:$destinationTarget\WindowsBlue-KB3191564-x64.cab"

    Exit
}


# Check and install AWS SDK
if (Get-Command "aws" -errorAction SilentlyContinue) {
    write-host("AWS SDK is already installed")
    aws configure set region ${REGION}
} else {
    write-host("Installing AWS SDK")
    Invoke-WebRequest -Uri 'https://s3.amazonaws.com/aws-cli/AWSCLI64PY3.msi' -Outfile 'AWSCLI64.msi'
    Start-Process "msiexec.exe" -Wait -NoNewWindow -ArgumentList '/I AWSCLI64.msi /quiet'
    $env:Path = [System.Environment]::GetEnvironmentVariable("Path","Machine") + ";" + [System.Environment]::GetEnvironmentVariable("Path","User")
}

# Check and prepare data volume.
if (Get-Disk -Number 1 -ErrorAction SilentlyContinue) {
    if (Get-PSDrive -PSProvider FileSystem -Name D -ErrorAction SilentlyContinue) {
        Write-Host 'The D: drive is already in use.'
    } else {
        # Create windows volume and format disk
        $DiskpartScript = 'select disk 1`r`nattributes disk clear readonly`r`nonline disk`r`nconvert mbr`r`ncreate partition primary`r`nformat quick fs=ntfs label="data"`r`nassign letter="D"'
        Set-Content -Path diskpart.txt -Value $DiskpartScript
        diskpart /s diskpart.txt
    }
} else {
    Write-Host "No Disk #1. Aborting script."
    Exit(1)
}

# Install dependencies
Install-PackageProvider -Name NuGet -MinimumVersion 2.8.5.201 -Force
Install-Module -Name PSWindowsUpdate -MinimumVersion 2.0.0.0 -Force
#Add-WUServiceManager -ServiceID 7971f918-a847-4430-9279-4a52d1efe18d -Confirm:$false
#Download-WindowsUpdate -MicrosoftUpdate -IgnoreUserInput -Verbose
#Get-WUInstall -MicrosoftUpdate -IgnoreUserInput -AcceptAll -AutoReboot -Verbose -install

# Check and install git
if (Get-Command "git" -errorAction SilentlyContinue) {
    write-host("git is already installed")
} else {
    write-host("Installing git")
    Invoke-WebRequest -Uri 'https://github.com/git-for-windows/git/releases/download/v2.21.0.windows.1/Git-2.21.0-64-bit.exe' -Outfile 'git-install.exe'
    Start-Process ".\git-install.exe" -Wait -NoNewWindow -ArgumentList '/verysilent'
}

# Check and install nuget
if (Get-Command "nuget" -errorAction SilentlyContinue) {
    write-host("nuget is already installed")
} else {
    $PATH = [Environment]::GetEnvironmentVariable("PATH")
    New-Item -ItemType Directory -Force -Path "C:\tools"
    $tools_path = "C:\tools"
    [Environment]::SetEnvironmentVariable("PATH", "$PATH;$tools_path", "Machine")

    write-host("Installing nuget")
    Invoke-WebRequest -Uri 'https://dist.nuget.org/win-x86-commandline/latest/nuget.exe' -Outfile 'C:\tools\nuget.exe'
}

# Configure ansible
$url = "https://raw.githubusercontent.com/ansible/ansible/devel/examples/scripts/ConfigureRemotingForAnsible.ps1"
$file = "$env:temp\ConfigureRemotingForAnsible.ps1"
(New-Object -TypeName System.Net.WebClient).DownloadFile($url, $file)
powershell.exe -ExecutionPolicy ByPass -File $file

# Check and install gitlab runner
$path = "D:\runner"
If (test-path $path) {
    write-host("Gitlab runner is already installed")
} else {
    New-Item -ItemType Directory -Force -Path $path
    Set-Location -Path $path
    Invoke-WebRequest -Uri 'https://gitlab-runner-downloads.s3.amazonaws.com/${RUNNER_VERSION}/binaries/gitlab-runner-windows-amd64.exe' -Outfile 'gitlab-runner.exe'
    .\gitlab-runner install

    .\gitlab-runner register  --non-interactive `
         --description "Win-#$RUNNER_ID on AWS-${ENV}" `
         --url "https://${DOMAIN}" `
         --limit "0" `
         --request-concurrency "${RUNNER_LIMIT}" `
         --registration-token ${REGISTRATION_TOKEN} `
         --run-untagged=false `
         --executor shell `
         --shell powershell `
         --cache-shared `
         --cache-path "${CACHE_PREFIX}" `
         --cache-s3-bucket-name "${CACHE_BUCKET}" `
         --tag-list "${TAGS}"
    .\gitlab-runner start

    Restart-Computer -Force
}

# Check and install msbuild
if (Get-Command "msbuild" -errorAction SilentlyContinue) {
    write-host("msbuild is already installed")
} else {
    write-host("Installing msbuild")
    $PATH = [Environment]::GetEnvironmentVariable("PATH")
    [Environment]::SetEnvironmentVariable("PATH", "$PATH;C:\Program Files (x86)\Microsoft Visual Studio\2017\Community\MSBuild\15.0\Bin", "Machine")
    Invoke-WebRequest -Uri 'https://download.visualstudio.microsoft.com/download/pr/11835057/045b56eb413191d03850ecc425172a7d/vs_Community.exe' -Outfile 'vs_community.exe'
    .\vs_community.exe /finalizeInstall --includeRecommended --passive `
        --add Microsoft.VisualStudio.Workload.NetWeb `
        --add Microsoft.VisualStudio.Workload.ManagedDesktop `
        --add Microsoft.VisualStudio.Component.NuGet.BuildTools `
        --add Microsoft.Net.Component.4.6.1.TargetingPack
}
