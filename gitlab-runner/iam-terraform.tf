data "aws_iam_policy_document" "gitlab-runner-terraform-assume" {
  count   = local.create-instances
  version = "2012-10-17"

  statement {
    effect = "Allow"
    principals {
      identifiers = [aws_iam_role.gitlab-runner[0].arn]
      type        = "AWS"
    }
    actions = ["sts:AssumeRole"]
  }
}

resource "aws_iam_role" "gitlab-runner-terraform" {
  count = local.create-instances
  name  = "${local.name}-terraform"
  path  = "/"

  permissions_boundary = var.iam_boundary
  assume_role_policy   = data.aws_iam_policy_document.gitlab-runner-terraform-assume[0].json

  tags = local.tags
}

data "aws_iam_policy_document" "gitlab-runner-terraform" {
  count   = local.create-instances
  version = "2012-10-17"

  statement {
    effect    = "Allow"
    actions   = ["*"]
    resources = ["*"]
  }
}

resource "aws_iam_role_policy" "gitlab-runner-terraform" {
  count = local.create-instances
  name  = "${local.name}-terraform"
  role  = aws_iam_role.gitlab-runner-terraform[0].id

  policy = data.aws_iam_policy_document.gitlab-runner-terraform[0].json
}

