resource "aws_iam_instance_profile" "gitlab-runner" {
  count = local.create-instances
  name  = local.name
  role  = aws_iam_role.gitlab-runner[0].name
}

data "aws_iam_policy_document" "gitlab-runner-ec2-assume" {
  count   = local.create-instances
  version = "2012-10-17"

  statement {
    effect = "Allow"
    principals {
      identifiers = ["ec2.${data.aws_partition.current.dns_suffix}"]
      type        = "Service"
    }
    actions = ["sts:AssumeRole"]
  }
}

resource "aws_iam_role" "gitlab-runner" {
  count = local.create-instances
  name  = local.name
  path  = "/"

  permissions_boundary = var.iam_boundary
  assume_role_policy   = data.aws_iam_policy_document.gitlab-runner-ec2-assume[0].json

  tags = local.tags
}

resource "aws_iam_role_policy" "gitlab-runner" {
  count = local.create-instances
  name  = local.name
  role  = aws_iam_role.gitlab-runner[0].id

  policy = <<EOF
{
  "Version": "2012-10-17",
  "Statement": [
    {
      "Sid": "nodesS3",
      "Effect": "Allow",
      "Action": [
        "s3:Get*",
        "s3:Put*"
      ],
      "Resource": "arn:${data.aws_partition.current.partition}:s3:::${var.infra_bucket}/*"
    },
    {
      "Sid": "nodesS3Cache",
      "Effect": "Allow",
      "Action": [
        "s3:Get*",
        "s3:Put*"
      ],
      "Resource": "arn:${data.aws_partition.current.partition}:s3:::${var.runner_cache_bucket}/*"
    },
    {
      "Sid": "nodesEcr",
      "Effect": "Allow",
      "Action": [
        "ecr:GetAuthorizationToken",
        "ecr:GetDownloadUrlForLayer",
        "ecr:BatchGetImage",
        "ecr:BatchCheckLayerAvailability",
        "ecr:PutImage",
        "ecr:InitiateLayerUpload",
        "ecr:UploadLayerPart",
        "ecr:CompleteLayerUpload"
      ],
      "Resource": [
        "*"
      ]
    },
    {
      "Sid": "nodesLogs",
      "Effect": "Allow",
      "Action": [
        "logs:CreateLogStream",
        "logs:PutLogEvents",
        "logs:DescribeLogStreams",
        "logs:GetLogEvents"
      ],
      "Resource": [
        "arn:${data.aws_partition.current.partition}:logs:${var.aws_region}:${data.aws_caller_identity.current.account_id}:log-group:/${aws_cloudwatch_log_group.gitlab-runner.name}/*:log-stream:*"
      ]
    },
    {
      "Sid": "nodesEcs",
      "Effect": "Allow",
      "Action": [
        "ecs:List*",
        "ecs:Describe*",
        "iam:PassRole",
        "ecs:UpdateService",
        "ecs:RegisterTaskDefinition"
      ],
      "Resource": [
        "*"
      ]
    },
    {
      "Sid": "nodesLambda",
      "Effect": "Allow",
      "Action": [
        "lambda:UpdateFunctionCode"
      ],
      "Resource": [
        "*"
      ]
    },
    {
      "Action": [
        "ec2:DescribeTags"
      ],
      "Resource": "*",
      "Effect": "Allow"
    },
    {
      "Action": [
        "ec2:DescribeInstances"
      ],
      "Resource": "*",
      "Effect": "Allow"
    },
    {
      "Action": [
        "ssm:DescribeAssociation",
        "ssm:GetDocument",
        "ssm:ListAssociations",
        "ssm:UpdateAssociationStatus",
        "ssm:UpdateInstanceInformation",
        "ssm:ListInstanceAssociations"
      ],
      "Resource": "*",
      "Effect": "Allow"
    },
    {
      "Action": [
        "ec2messages:AcknowledgeMessage",
        "ec2messages:DeleteMessage",
        "ec2messages:FailMessage",
        "ec2messages:GetEndpoint",
        "ec2messages:GetMessages",
        "ec2messages:SendReply"
      ],
      "Resource": "*",
      "Effect": "Allow"
    },
    {
      "Sid": "tmpAdmin",
      "Effect": "Allow",
      "Action": [
        "*"
      ],
      "Resource": [
        "*"
      ]
    }
  ]
}
EOF

}

