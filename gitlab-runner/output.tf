output "terraform_role" {
  value = ""
}

output "runners" {
  value = aws_route53_record.main.*.name
}

output "sg_id" {
  value = local.create-instances > 0 ? aws_security_group.gitlab-runner[0].id : null
}

output "iam_role_arn" {
  value = local.create-instances > 0 ? aws_iam_role.gitlab-runner[0].id : null
}

output "windows_passwords" {
  value = random_string.windows-password.*.result
}

