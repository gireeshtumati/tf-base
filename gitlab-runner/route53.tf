resource "aws_route53_record" "main" {
  count    = var.dns_create ? var.runner_count : 0
  provider = aws.route53
  zone_id  = var.dns_zone
  name = "${format(
    "%s-%s-%d",
    "gitlab-runner",
    var.runner_type,
    count.index + 1
  )}.${var.dns_suffix}"
  type = "A"

  records = [element(aws_instance.gitlab-runner.*.private_ip, count.index)]
  ttl     = "300"
}

