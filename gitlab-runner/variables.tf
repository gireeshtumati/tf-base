variable "ami" {
  type = string
}

variable "aws_region" {
  type = string
}

variable "aws_zones" {
  type = list(string)
}

variable "aws_vpc_id" {
  type = string
}

variable "gitlab_domain" {
  type = string
}

variable "aws_vpc_subnets" {
  type = list(string)
}

variable "infra_bucket" {
  type = string
}

variable "infra_prefix" {
  type = string
}

variable "runner_count" {
  type = string
}

variable "runner_type" {
  type = string
}

variable "runner_registration_token" {
  type = string
}

variable "runner_instance_type" {
  type = string
}

variable "runner_version" {
  type = string
}

variable "runner_limit" {
  type = string
}

variable "runner_root_disk_size" {
  type = string
}

variable "runner_disk_size" {
  type = string
}

variable "runner_disk_inodes" {
  type    = string
  default = ""
}

variable "runner_cache_prefix" {
  type = string
}

variable "dns_create" {
  type    = bool
  default = false
}

variable "dns_zone" {
  default = ""
}

variable "dns_suffix" {
  default = ""
}

variable "runner_tags" {
  type    = list(string)
  default = []
}

variable "runner_cache_bucket" {
  type = string
}

variable "key_name" {
  type = string
}

variable "iam_boundary" {
  type = string
}

variable "environment" {
  type = string
}

variable "tags" {
  type    = map(string)
  default = {}
}

variable "module_prefix" {
  default = ""
}

