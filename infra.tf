resource "aws_s3_bucket" "infra" {
  bucket = var.infra_bucket

  versioning {
    enabled = true
  }

  lifecycle {
    prevent_destroy = true
  }

  tags = merge(local.tags, {
    format(local.shared_tag, "role") = "Shared resources"
  })
}

resource "aws_dynamodb_table" "infra" {
  name         = "hilti-infra-terraform"
  hash_key     = "LockID"
  billing_mode = "PAY_PER_REQUEST"

  attribute {
    name = "LockID"
    type = "S"
  }

  lifecycle {
    prevent_destroy = true

    ignore_changes = [
      read_capacity,
      write_capacity,
    ]
  }

  tags = merge(local.tags, {
    format(local.shared_tag, "role") = "Shared resources"
  })
}

resource "aws_key_pair" "user" {
  key_name   = "${var.module_prefix}admin"
  public_key = var.user_key
}

