locals {
  addon_cluster_autoscaler = {
    chart_version = "3.2.0"
    name          = "cluster-autoscaler"
    namespace     = local.system_srv_namespace
    helm_values   = {
      image = {
        repository = split(":", local.urls_mappings[data.aws_partition.current.partition].cluster_autoscaler)[0]
        tag        = split(":", local.urls_mappings[data.aws_partition.current.partition].cluster_autoscaler)[1]
      }

      sslCertPath = "/etc/ssl/certs/ca-bundle.crt"

      autoDiscovery = {
        clusterName = !var.create ? "" : aws_eks_cluster.this[0].name
      }

      extraArgs = {
        scale-down-delay-after-add = "1m0s"
        scale-down-unneeded-time   = "1m0s"
      }

      priorityClassName = "system-cluster-critical"

      podAnnotations = {
        "iam.amazonaws.com/role" = !var.create ? "" : aws_iam_role.cluster-autoscaler[0].arn
      }

      awsRegion = var.aws_region

      cloudProvider = "aws"

      rbac = {
        create = true
      }

      tolerations  = local.system_srv_tolerations
      nodeSelector = local.system_srv_node_selector

      resources = {
        requests = {
          cpu    = "100m"
          memory = "300Mi"
        }
        limits   = {
          cpu    = "100m"
          memory = "300Mi"
        }
      }
    }
  }
}

resource "aws_iam_role" "cluster-autoscaler" {
  count = local.create_resources

  name = "${local.name}-cluster-autoscaler"

  assume_role_policy = data.aws_iam_policy_document.cluster-autoscaler-assume[0].json

  permissions_boundary = var.iam_boundary
}

data "aws_iam_policy_document" "cluster-autoscaler-assume" {
  count = local.create_resources

  version = "2012-10-17"

  statement {
    actions = [
      "sts:AssumeRole"]

    principals {
      type        = "AWS"
      identifiers = [
        module.ig-shared-resources.iam_nodes_role_arn]
    }
  }

  statement {
    actions = [
      "sts:AssumeRole"]

    principals {
      type        = "Service"
      identifiers = [
        "ec2.${data.aws_partition.current.dns_suffix}"
      ]
    }
  }
}

data "aws_iam_policy_document" "cluster-autoscaler" {
  count = local.create_resources

  version = "2012-10-17"

  statement {
    actions = [
      "autoscaling:DescribeAutoScalingGroups",
      "autoscaling:DescribeAutoScalingInstances",
      "autoscaling:DescribeLaunchConfigurations",
      "autoscaling:DescribeTags",
      "autoscaling:SetDesiredCapacity",
      "autoscaling:TerminateInstanceInAutoScalingGroup",
    ]

    effect = "Allow"

    resources = [
      "*"]
  }
}

resource "aws_iam_policy" "cluster-autoscaler" {
  count = local.create_resources

  name = "${local.name}-cluster-autoscaler"

  policy = data.aws_iam_policy_document.cluster-autoscaler[0].json
}

resource "aws_iam_role_policy_attachment" "cluster-autoscaler" {
  count      = local.create_resources
  role       = aws_iam_role.cluster-autoscaler[0].name
  policy_arn = aws_iam_policy.cluster-autoscaler[0].arn
}
