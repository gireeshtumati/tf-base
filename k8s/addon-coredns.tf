locals {
  addon_coredns = {
    deployment = jsonencode({
      spec = {
        template = {
          spec = {
            tolerations  = local.system_srv_tolerations
            nodeSelector = local.system_srv_node_selector
          }
        }
      }
    })
  }
}
