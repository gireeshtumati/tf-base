locals {
  addon_helm = {
    tiller = {
      image = local.urls_mappings[data.aws_partition.current.partition].helm_tiller
      namespace  = local.system_srv_namespace
      deployment = jsonencode({
        spec = {
          template = {
            spec = {
              serviceAccount = "tiller"
              tolerations    = local.system_srv_tolerations
              nodeSelector   = local.system_srv_node_selector
            }
          }
        }
      })
    }
  }
}
