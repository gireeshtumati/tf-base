locals {
  addon_ingress = {
    chart_version = "1.11.5"
    name          = "nginx-ingress"
    namespace     = local.system_srv_namespace
    helm_values   = {
      rbac = {
        create = true
      }

      tcp = local.ingress_tcp_mappings
      udp = {}

      controller = {
        image = {
          repository = split(":", local.urls_mappings[data.aws_partition.current.partition].nginx_ingress_controller)[0]
          tag        = split(":", local.urls_mappings[data.aws_partition.current.partition].nginx_ingress_controller)[1]
        }

        # https://github.com/kubernetes/ingress-nginx/blob/master/docs/user-guide/nginx-configuration/configmap.md
        config = {
          disable-ipv6 = "true"
          use-http2    = "true"
          // use-proxy-protocol = "true"

          compute-full-forwarded-for = "true"

          // ssl-dh-param = ""
          ssl-ecdh-curve      = "secp384r1"
          ssl-ciphers         = "ECDHE-RSA-AES128-GCM-SHA256:ECDHE-ECDSA-AES128-GCM-SHA256:ECDHE-RSA-AES256-GCM-SHA384:ECDHE-ECDSA-AES256-GCM-SHA384:DHE-RSA-AES128-GCM-SHA256:DHE-DSS-AES128-GCM-SHA256:kEDH+AESGCM:ECDHE-RSA-AES128-SHA256:ECDHE-ECDSA-AES128-SHA256:ECDHE-RSA-AES128-SHA:ECDHE-ECDSA-AES128-SHA:ECDHE-RSA-AES256-SHA384:ECDHE-ECDSA-AES256-SHA384:ECDHE-RSA-AES256-SHA:ECDHE-ECDSA-AES256-SHA:DHE-RSA-AES128-SHA256:DHE-RSA-AES128-SHA:DHE-DSS-AES128-SHA256:DHE-RSA-AES256-SHA256:DHE-DSS-AES256-SHA:DHE-RSA-AES256-SHA:AES128-GCM-SHA256:AES256-GCM-SHA384:AES128-SHA256:AES256-SHA256:AES128-SHA:AES256-SHA:AES:CAMELLIA:DES-CBC3-SHA:!aNULL:!eNULL:!EXPORT:!DES:!RC4:!MD5:!PSK:!aECDH:!EDH-DSS-DES-CBC3-SHA:!EDH-RSA-DES-CBC3-SHA:!KRB5-DES-CBC3-SHA"
          ssl-protocols       = "TLSv1.2 TLSv1.3"
          ssl-session-cache   = "shared:SSL:10m"
          ssl-session-tickets = "off"
          ssl-session-timeout = "10m"

          keep-alive: "1200"
          proxy-stream-timeout: "1200"
          proxy-read-timeout: "600"
          proxy-send-timeout: "600"
          proxy-request-buffering: "off"
          worker-shutdown-timeout: "120s"
        }

        hostNetwork = false
      
        kind = "Deployment"

        autoscaling = {
          enabled     = true
          minReplicas = 2
          maxReplicas = 5
        }

        tolerations  = local.system_srv_tolerations
        nodeSelector = local.system_srv_node_selector

        resources = {
          requests = {
            cpu    = "500m"
            memory = "256Mi"
          }
          limits   = {
            cpu    = "2000m"
            memory = "2048Mi"
          }
        }

        service = {              
          annotations              = {
            "service.beta.kubernetes.io/aws-load-balancer-internal" = "0.0.0.0/0"
            "service.beta.kubernetes.io/aws-load-balancer-ssl-cert" = data.aws_acm_certificate.hilti_cloud.arn
            "service.beta.kubernetes.io/aws-load-balancer-ssl-ports" = "https"
            "service.beta.kubernetes.io/aws-load-balancer-backend-protocol" = "https"
          }
          loadBalancerSourceRanges = [
            "0.0.0.0/0",
          ]
        }

        metrics = {
          enabled = true
          service = {
            annotations = {}
          }
        }
      }

      defaultBackend = {
        enabled = true

        image = {
          repository = split(":", local.urls_mappings[data.aws_partition.current.partition].default_backend)[0]
          tag        = split(":", local.urls_mappings[data.aws_partition.current.partition].default_backend)[1]
        }

        tolerations  = local.system_srv_tolerations
        nodeSelector = local.system_srv_node_selector

        resources = {
          requests = {
            cpu    = "50m"
            memory = "64Mi"
          }
          limits   = {
            cpu    = "500m"
            memory = "256Mi"
          }
        }
      }
    }
  }
}

# https://aws.amazon.com/blogs/networking-and-content-delivery/using-static-ip-addresses-for-application-load-balancers/
module "ingress-alb-linker" {
  source = "./ingress-alb-linker"

  create = var.create

  name          = "${local.name}-ingress-alb-linker"
  environment   = var.environment
  aws_region    = var.aws_region
  alb_listeners = [for tg in aws_lb_target_group.ingress : {
    port = tg.port
    tg   = tg.arn
  }]

  vpc_id            = var.vpc_id
  vpc_subnets       = var.vpc_subnets
  vpc_subnets_cidrs = var.vpc_subnets_cidrs

  iam_boundary       = var.iam_boundary
  infra_bucket       = var.infra_bucket
  infra_prefix       = var.infra_prefix
  shared_tag         = var.shared_tag
  module_prefix      = var.module_prefix
  use_public_network = var.use_public_network
  env                = var.env
  lambda_stub_bucket = var.lambda_stub_bucket
  lambda_stub_key    = var.lambda_stub_key
}

resource "aws_route53_record" "ingress-lb" {
  count = local.create_resources

  provider = aws.route53
  zone_id  = var.dns_zone
  name     = local.ingress_dns

  type = "CNAME"

  ttl = 60

  # TODO: Temporary
  records = [
    aws_lb.ingress[0].dns_name,
  ]
}
