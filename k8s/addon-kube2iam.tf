locals {
  # https://github.com/helm/charts/tree/master/stable/kube2iam
  addon_kube2iam = {
    chart_version = "2.0.0"
    name          = "kube2iam"
    namespace     = local.system_srv_namespace
    helm_values   = {
      image = {
        repository = split(":", local.urls_mappings[data.aws_partition.current.partition].kube2iam)[0]
        tag        = split(":", local.urls_mappings[data.aws_partition.current.partition].kube2iam)[1]
      }

      host = {
        iptables  = true
        interface = "eni+"
      }

      rbac = {
        create = true
      }

      extraEnv = {
        AWS_REGION = var.aws_region
      }

      extraArgs = {
        base-role-arn = "arn:${data.aws_partition.current.partition}:iam::${var.env.account_id}:role/"
        default-role  = module.ig-shared-resources.iam_default_pod_role_arn

        use-regional-sts-endpoint = ""

        namespace-key                = "iam.amazonaws.com/allowed-roles"
        namespace-restrictions       = ""
        namespace-restriction-format = "regexp"
      }

      priorityClassName = "system-node-critical"

      tolerations  = local.system_srv_tolerations_all
      nodeSelector = {}

      resources = {
        requests = {
          cpu    = "100m"
          memory = "128Mi"
        }
        limits   = {
          cpu    = "200m"
          memory = "256Mi"
        }
      }
    }
  }
}
