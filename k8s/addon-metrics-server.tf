locals {
  # https://github.com/helm/charts/tree/master/stable/metrics-server
  addon_metrics_server = {
    chart_version = "2.8.2"
    name          = "metrics-server"
    namespace     = local.system_srv_namespace
    helm_values   = {
      image = {
        repository = split(":", local.urls_mappings[data.aws_partition.current.partition].metrics_server)[0]
        tag        = split(":", local.urls_mappings[data.aws_partition.current.partition].metrics_server)[1]
      }

      sslCertPath = "/etc/ssl/certs/ca-bundle.crt"

      annotations = {
        "cluster-autoscaler.kubernetes.io/safe-to-evict" = "true"
      }

      args = [
        "--kubelet-insecure-tls",
        "--kubelet-preferred-address-types=InternalIP",
      ]

      rbac = {
        create = true
      }

      tolerations  = local.system_srv_tolerations
      nodeSelector = local.system_srv_node_selector

      resources = {
        requests = {
          cpu    = "5m"
          memory = "50Mi"
        }
        limits   = {
          cpu    = "100m"
          memory = "300Mi"
        }
      }
    }
  }
}
