locals {
  addon_prometheus = {
    chart_version = "11.5.0"
    name          = "prometheus"
    namespace     = local.system_srv_namespace
    helm_values   = {
      server = {
        image = {
          repository = split(":", local.urls_mappings[data.aws_partition.current.partition].prometheus)[0]
          tag        = split(":", local.urls_mappings[data.aws_partition.current.partition].prometheus)[1]
        }

        persistentVolume = {
          size         = "80Gi"
          storageClass = "eu-west-1c-sc"
        }

        tolerations  = local.system_srv_tolerations
        nodeSelector = local.system_srv_node_selector

        resources = {
          requests = {
            cpu    = "300m"
            memory = "512Mi"
          }
          limits   = {
            cpu    = "900m"
            memory = "1Gi"
          }
        }
      }

      alertmanager = {
        image = {
          repository = split(":", local.urls_mappings[data.aws_partition.current.partition].alertmanager)[0]
          tag        = split(":", local.urls_mappings[data.aws_partition.current.partition].alertmanager)[1]
        }

        tolerations  = local.system_srv_tolerations
        nodeSelector = local.system_srv_node_selector

        persistentVolume = {
          size         = "8Gi"
          storageClass = "eu-west-1c-sc"
        }

        resources = {
          requests = {
            cpu    = "100m"
            memory = "300Mi"
          }
          limits   = {
            cpu    = "300m"
            memory = "500Mi"
          }
        }
      }

      pushgateway = {
        image = {
          repository = split(":", local.urls_mappings[data.aws_partition.current.partition].pushgateway)[0]
          tag        = split(":", local.urls_mappings[data.aws_partition.current.partition].pushgateway)[1]
        }

        tolerations  = local.system_srv_tolerations
        nodeSelector = local.system_srv_node_selector

        persistentVolume = {
          size         = "8Gi"
          storageClass = "eu-west-1c-sc"
        }

        resources = {
          requests = {
            cpu    = "200m"
            memory = "512Mi"
          }
          limits   = {
            cpu    = "500m"
            memory = "4Gi"
          }
        }
      }

      kubeStateMetrics = {
        image = {
          repository = split(":", local.urls_mappings[data.aws_partition.current.partition].kube_state_metrics)[0]
          tag        = split(":", local.urls_mappings[data.aws_partition.current.partition].kube_state_metrics)[1]
        }

        tolerations  = local.system_srv_tolerations
        nodeSelector = local.system_srv_node_selector

        resources = {
          requests = {
            cpu    = "100m"
            memory = "300Mi"
          }
          limits   = {
            cpu    = "300m"
            memory = "500Mi"
          }
        }
      }
    }
  }
}
