module "k8s-ng" {
  source = "https://s3-eu-west-1.amazonaws.com/hilti-infra-public/shared-artifacts/terraform-modules/k8s-ng/0.1.0-master.zip"

  create = var.create

  project     = null
  service     = "k8s"
  name        = "system"
  environment = var.environment

  tags = local.tags

  ec2_instance_type    = "t3.medium"
  ec2_sg_min           = 1
  ec2_sg_max           = 5
  ec2_root_volume_size = 50

  ssh_keys = [
    "ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAACAQC3FRxGJgBt2pE8aN4MULpLhEMZoBVHC10TaVFpHYDxTLpbVuVUAB/iNKilYjYpBvxtGo/Yv9mu/++i933kNEs7lSMqmhTdn6I1Ftel3UoFDnOAbrs3S/gVFns23bUrAvNcmJOGNjTxiU3VwmPbJwgPd75+K4rkp7VkfOp4eI1B0AJNK5WNwv6bmeVp5/eL+6EUaVI3R2bStWFTqIHVtmSW4oDWUNVbKb9TfnTaseUJsY997OtQ7oCmOvGiWOaGPEZ1cEyDg828uryeNuQ5DE4wY522PNk4l4JAd5Ft/fnHRfz0IHjVkrUqeQJXMmvWfLFLWjm4xH3y63EU6E2jRFFUkS1gA5yu5pBi6em7i8ScgaR4V6Jp8UGXXs+Qn/385aZlG6rT05TBXLe7lI700zxR5NVKKCqycky3gIjuYtomso8YDlwV8KPKo0OFFTR1Ys8WmrNTXVQaksiSC6gjHJpMZv1wLvJlFWt9QUcysJqCerpH6ZD1cMnz9hleGTvejPU/dYNpJH3nekEtVj+dFEjsK15TEVCQJuRwGaLwC2BJONf2ksT431FLzUulYitNloCtj6wqLOElf1AigvNK1CQDDQNmxF3ukGN9azcvcpba6n3K9Vdkc/WYoa+OdGN7C9UF24Yw+BUnzoJUOhlmsUzzjMf29jy4j7ozUVQ28oQNAQ==",
  ]

  use_remote_state    = false
  module_prefix       = var.module_prefix
  env                 = var.env
  shared_tag          = var.shared_tag
  vpc_id              = var.vpc_id
  vpc_subnets         = var.vpc_subnets
  permission_boundary = var.iam_boundary
  infra_bucket        = var.infra_bucket
  ami_eks_linux       = var.create ? data.aws_ami.eks_linux[0].id : ""
  k8s_cluster_name    = local.k8s_name

  k8s_node_labels = local.system_srv_node_selector
  k8s_node_taints = [for item in local.system_srv_tolerations : {
    key    = item.key
    value  = item.value
    effect = item.effect
  }]

  k8s_ec2_nodes_sg_id     = module.ig-shared-resources.ec2_nodes_sg_id
  k8s_iam_nodes_role_name = module.ig-shared-resources.iam_nodes_role_name
}
