resource "aws_security_group" "ec2-nodes" {
  count = local.create_resources

  name        = "${local.name}-ec2-nodes"
  description = "Security group for k8s nodes"
  vpc_id      = var.vpc_id

  tags = local.tags
}

resource "aws_security_group_rule" "ec2-nodes-egress" {
  count             = local.create_resources
  description       = "Allows ${local.name}-ec2-nodes to talk to anything"
  type              = "egress"
  security_group_id = aws_security_group.ec2-nodes[0].id
  from_port         = 0
  to_port           = 0
  protocol          = "-1"
  cidr_blocks       = ["0.0.0.0/0"]
}

resource "aws_security_group_rule" "ec2-nodes-internal" {
  count                    = local.create_resources
  description              = "Allow ${local.name}-ec2-nodes to communicate with each other"
  type                     = "ingress"
  security_group_id        = aws_security_group.ec2-nodes[0].id
  source_security_group_id = aws_security_group.ec2-nodes[0].id
  from_port                = 0
  to_port                  = 0
  protocol                 = "-1"
}

resource "aws_security_group_rule" "ec2-nodes-controlpane-ingress" {
  count                    = local.create_resources
  description              = "Allow worker Kubelets and pods to receive communication from the cluster control plane"
  type                     = "ingress"
  security_group_id        = aws_security_group.ec2-nodes[0].id
  source_security_group_id = var.sg_master_id
  from_port                = 1025
  to_port                  = 65535
  protocol                 = "tcp"
}

resource "aws_security_group_rule" "ec2-nodes-consumers-ingress" {
  count                    = local.create_resources * var.sg_sources_count
  description              = "Allow worker Kubelets and pods to receive communication from consumers"
  type                     = "ingress"
  security_group_id        = aws_security_group.ec2-nodes[0].id
  source_security_group_id = var.sg_sources_ids[count.index]
  from_port                = 1025
  to_port                  = 65535
  protocol                 = "tcp"
}

resource "aws_security_group_rule" "ec2-nodes-controlpane-ingress-443" {
  count                    = local.create_resources
  description              = "Allow pods running extension API servers on port 443 to receive communication from cluster control plane"
  type                     = "ingress"
  security_group_id        = aws_security_group.ec2-nodes[0].id
  source_security_group_id = var.sg_master_id
  from_port                = 443
  to_port                  = 443
  protocol                 = "tcp"
}

resource "aws_security_group_rule" "ec2-nodes-ingress-22" {
  count             = local.create_resources
  description       = "Allow SSH access"
  type              = "ingress"
  security_group_id = aws_security_group.ec2-nodes[0].id
  from_port         = 22
  to_port           = 22
  protocol          = "tcp"
  cidr_blocks       = ["0.0.0.0/0"]
}
