locals {
  name       = var.name
  local_tags = {
    Name = local.name
  }
  tags       = merge(var.tags, local.local_tags)
}

locals {
  iam_role_name = var.create ? element(split("*", join("*", [aws_iam_role.ec2-nodes[0].arn, aws_iam_role.ec2-nodes[0].name])), 1) : null
}
