variable "create" {
  type    = bool
  default = false
}

locals {
  create_resources = var.create ? 1 : 0
}

variable "tags" {
  type    = map(string)
  default = {}
}

variable "sg_master_id" {
  type = string
}

variable "iam_boundary" {
  type = string
}

variable "sg_sources_ids" {
  type = list(string)
}

variable "sg_sources_count" {
  type    = number
  default = 0
}

variable "name" {
  type = string
}

variable "vpc_id" {
  type = string
}
