module "lambda" {
  source = "https://s3-eu-west-1.amazonaws.com/hilti-infra-public/shared-artifacts/terraform-modules/lambda/0.1.0-master.zip"

  create = var.create

  use_remote_state    = false
  module_prefix       = var.module_prefix
  env                 = var.env
  shared_tag          = var.shared_tag
  vpc_id              = var.vpc_id
  vpc_subnets         = var.vpc_subnets
  vpc_subnets_cidrs   = var.vpc_subnets_cidrs
  infra_bucket        = var.infra_bucket
  infra_prefix        = var.infra_prefix
  permission_boundary = var.iam_boundary
  use_public_network  = var.use_public_network
  lambda_stub_bucket  = var.lambda_stub_bucket
  lambda_stub_key     = var.lambda_stub_key
  aws_region          = var.aws_region

  lambda_runtime = "python2.7"
  lambda_handler = "main.lambda_handler"

  subscription_cron_rule = "rate(1 minute)"

  project     = null
  service     = "infra"
  name        = var.name
  environment = var.environment

  lambda_variables = {
    ALB_DNS_NAME                      = ""
    ALB_LISTENERS                     = join(",", [for row in var.alb_listeners : row.port])
    S3_BUCKET                         = var.create ? aws_s3_bucket.this[0].bucket : ""
    NLB_TG_ARNS                       = join(",", [for row in var.alb_listeners : row.tg])
    MAX_LOOKUP_PER_INVOCATION         = 50
    INVOCATIONS_BEFORE_DEREGISTRATION = 3
    CW_METRIC_FLAG_IP_COUNT           = true
  }

  tags = var.tags
}
