resource "aws_s3_bucket" "this" {
  count = local.create_resources

  bucket = "${var.name}-${var.environment}"

  region = var.aws_region

  tags = var.tags
}
