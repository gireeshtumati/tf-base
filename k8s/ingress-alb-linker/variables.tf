variable "create" {
  type    = bool
}

locals {
  create_resources = var.create ? 1 : 0
}

variable "tags" {
  type    = map(string)
  default = {}
}

variable "environment" {
  type = string
}

variable "name" {
  type = string
}

variable "use_public_network" {
  type = bool
}

variable "env" {
  type = object({
    account_id = string
  })
}

variable "aws_region" {
  type = string
}

variable "vpc_id" {
  type = string
}

variable "vpc_subnets" {
  type = list(string)
}

variable "vpc_subnets_cidrs" {
  type = list(string)
}

variable "iam_boundary" {
  type = string
}

variable "module_prefix" {
  type = string
}

variable "shared_tag" {
  type = string
}

variable "infra_bucket" {
  type = string
}

variable "infra_prefix" {
  type = string
}

variable "lambda_stub_key" {
  type = string
}

variable "lambda_stub_bucket" {
  type = string
}

variable "alb_listeners" {
  type = list(object({
    port = number
    tg = string
  }))
}
