locals {
  # https://istio.io/docs/reference/config/istio.operator.v1alpha12.pb/#AutoInjectionFeatureSpec
  feature_autoinjection = {
    enabled = true

    components = {
      # https://istio.io/docs/reference/config/istio.operator.v1alpha12.pb/#SidecarInjectorComponentSpec
      injector = {
        enabled = true

        k8s = merge(local.k8s_default_config, {})
      }
    }
  }
}
