locals {
  # https://istio.io/docs/reference/config/istio.operator.v1alpha12.pb/#ConfigManagementFeatureSpec
  feature_configmanagement = {
    enabled = true

    components = {
      # https://istio.io/docs/reference/config/istio.operator.v1alpha12.pb/#GalleyComponentSpec
      galley = {
        enabled = true

        k8s = merge(local.k8s_default_config, {
          resources = {
            requests = {
              cpu = "100m"
            }
          }
        })
      }
    }
  }
}
