locals {
  # https://istio.io/docs/tasks/observability/metrics/using-istio-dashboard/
  # https://istio.io/docs/reference/config/installation-options/#grafana-options
  values_grafana = {
    enabled = true

    replicaCount = 1

    persist = true

    nodeSelector = local.k8s_default_config.nodeSelector
    tolerations  = local.k8s_default_config.tolerations
  }

  # https://istio.io/docs/reference/config/installation-options/#kiali-options
  values_kiali = {
    enabled = true

    replicaCount = 1

    nodeSelector = local.k8s_default_config.nodeSelector
    # https://github.com/istio/istio/issues/19480
    //tolerations  = local.k8s_default_config.tolerations
  }

  # https://istio.io/docs/reference/config/policy-and-telemetry/adapters/prometheus/
  # https://istio.io/docs/reference/config/installation-options/#prometheus-options
  values_prometheus = {
    enabled = true

    replicaCount = 1

    nodeSelector = local.k8s_default_config.nodeSelector
    tolerations  = local.k8s_default_config.tolerations
  }

  # https://istio.io/docs/reference/config/installation-options/#tracing-options
  values_tracing = {
    enabled = true

    provider = "jaeger"

    nodeSelector = local.k8s_default_config.nodeSelector
    # https://github.com/istio/istio/issues/19480
    //tolerations  = local.k8s_default_config.tolerations

    // pod has unbound immediate PersistentVolumeClaims
    // jaeger = {
    //   persist = true
    // }
  }
}
