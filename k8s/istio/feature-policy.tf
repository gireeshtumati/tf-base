locals {
  # https://istio.io/docs/reference/config/istio.operator.v1alpha12.pb/#PolicyFeatureSpec
  feature_policy = {
    enabled = true

    components = {
      # https://istio.io/docs/reference/config/istio.operator.v1alpha12.pb/#PolicyComponentSpec
      policy = {
        enabled = true

        k8s = merge(local.k8s_default_config, {
          env = [
            {
              name      = "POD_NAMESPACE"
              valueFrom = {
                fieldRef = {
                  apiVersion = "v1"
                  fieldPath  = "metadata.namespace"
                }
              }
            }
          ]

          hpaSpec = {
            minReplicas    = 2
            maxReplicas    = 5
            metrics        = [
              {
                resource = {
                  name                     = "cpu"
                  targetAverageUtilization = 80
                }
                type     = "Resource"
              }
            ]
            scaleTargetRef = {
              apiVersion = "apps/v1"
              kind       = "Deployment"
              name       = "istio-policy"
            }
          }

          resources = {
            requests = {
              cpu    = "10m"
              memory = "100Mi"
            }
            limits   = {
              cpu    = "2000m"
              memory = "1024Mi"
            }
          }
        })
      }
    }
  }
}
