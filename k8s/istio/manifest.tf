locals {
  # https://istio.io/docs/reference/config/istio.operator.v1alpha12.pb/#KubernetesResourcesSpec
  k8s_default_config = {
    replicaCount = 2

    strategy = {
      rollingUpdate = {
        maxSurge       = "100%"
        maxUnavailable = "25%"
      }
    }

    resources = {
      requests = {
        cpu    = "50m"
        memory = "64Mi"
      }
      limits   = {
        cpu    = "1000m"
        memory = "1024Mi"
      }
    }

    tolerations = local.system_srv_tolerations

    nodeSelector = local.system_srv_node_selector
  }

  # https://istio.io/docs/reference/config/istio.operator.v1alpha12.pb/#IstioControlPlaneSpec
  manifest = {
    apiVersion = "install.istio.io/v1alpha2"
    kind       = "IstioControlPlane"
    spec       = {
      defaultNamespace = var.k8s_namespace
      hub              = var.istio_hub
      tag              = var.istio_version
      //      installPackagePath = "/tmp/istio-installer"
      // profile = ""

      autoInjection     = local.feature_autoinjection
      cni               = local.feature_cni
      configManagement  = local.feature_configmanagement
      coreDNS           = local.feature_coredns
      gateways          = local.feature_gateways
      policy            = local.feature_policy
      security          = local.feature_security
      telemetry         = local.feature_telemetry
      trafficManagement = local.feature_trafficmanagement

      # https://github.com/istio/istio/blob/master/install/kubernetes/helm/istio/values.yaml
      values = {
        gateways     = local.values_gateways
        istiocoredns = local.values_coredns

        # https://istio.io/docs/reference/config/installation-options/#global-options
        global = {
          controlPlaneSecurityEnabled = true

          # https://istio.io/docs/tasks/traffic-management/ingress/ingress-certmgr/
          k8sIngress = {
            enabled     = true
            enableHttps = true
            gatewayName = "ingressgateway"
          }

          # https://istio.io/docs/concepts/security/#mutual-tls-authentication
          mtls = {
            # https://istio.io/docs/tasks/security/authentication/auto-mtls/
            auto    = true
            enabled = false
          }

          # https://github.com/istio/istio/issues/19480
          // defaultTolerations  = local.k8s_default_config.tolerations
          // defaultNodeSelector = local.k8s_default_config.nodeSelector
        }

        # https://istio.io/docs/reference/config/installation-options/#prometheus-options
        prometheus = local.values_prometheus

        # https://istio.io/docs/reference/config/installation-options/#grafana-options
        grafana = local.values_grafana

        # https://istio.io/docs/reference/config/installation-options/#kiali-options
        kiali = local.values_kiali

        # https://istio.io/docs/reference/config/installation-options/#tracing-options
        tracing = local.values_tracing

        certmanager = {
          enabled = false
        }
      }
    }
  }
}
