module "k8s-ns-disposable" {
  source = "https://s3-eu-west-1.amazonaws.com/hilti-infra-public/shared-artifacts/terraform-modules/k8s-ns/0.1.0-master.zip"

  create = var.create

  project     = null
  service     = "k8s"
  name        = "disposable"
  environment = var.environment

  ansible_directory_prefix = "artifacts/ansible/infra/20-"

  tags = local.tags

  use_remote_state       = false
  module_prefix          = var.module_prefix
  env                    = var.env
  permission_boundary    = var.iam_boundary
  shared_tag             = var.shared_tag
  k8s_cluster_name       = local.k8s_name
  k8s_iam_nodes_role_arn = module.ig-shared-resources.iam_nodes_role_arn
}

module "k8s-ng-disposable" {
  source = "https://s3-eu-west-1.amazonaws.com/hilti-infra-public/shared-artifacts/terraform-modules/k8s-ng/0.1.0-master.zip"

  create = var.create

  project     = null
  service     = "k8s"
  name        = "disposable"
  environment = var.environment

  tags = local.tags

  ec2_instance_type    = "t3.medium"
  ec2_sg_min           = 2
  ec2_sg_max           = 10
  ec2_root_volume_size = 20
  ec2_spot_price       = "0.3" // yen

  k8s_node_labels = module.k8s-ns-disposable.k8s_node_labels
  k8s_node_taints = module.k8s-ns-disposable.k8s_node_taints

  use_remote_state    = false
  module_prefix       = var.module_prefix
  env                 = var.env
  shared_tag          = var.shared_tag
  vpc_id              = var.vpc_id
  vpc_subnets         = var.vpc_subnets
  permission_boundary = var.iam_boundary
  infra_bucket        = var.infra_bucket
  ami_eks_linux       = var.create ? data.aws_ami.eks_linux[0].id : ""
  k8s_cluster_name    = local.k8s_name

  k8s_ec2_nodes_sg_id     = module.ig-shared-resources.ec2_nodes_sg_id
  k8s_iam_nodes_role_name = module.ig-shared-resources.iam_nodes_role_name
}
