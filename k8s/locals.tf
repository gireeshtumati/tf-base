locals {
  name       = var.name
  local_tags = {
    Name = local.name
  }
  tags       = merge(var.tags, local.local_tags)
}

locals {
  data_dir = "${path.module}/data"

  urls_mappings = {
    aws    = {
      helm_tiller                    = "gcr.io/kubernetes-helm/tiller:v2.16.1"
      cluster_autoscaler             = "k8s.gcr.io/cluster-autoscaler:v1.13.1"
      metrics_server                 = "gcr.io/google_containers/metrics-server-amd64:v0.3.2"
      nginx_ingress_controller       = "quay.io/kubernetes-ingress-controller/nginx-ingress-controller:0.25.0"
      default_backend                = "k8s.gcr.io/defaultbackend-amd64:1.5"
      cert_manager_controller        = "quay.io/jetstack/cert-manager-controller:v0.6.2"
      prometheus_cloudwatch_exporter = "prom/cloudwatch-exporter:cloudwatch_exporter-0.8.0"
      kibana_oss                     = "docker.elastic.co/kibana/kibana-oss:6.7.0"
      kubernetes_dashboard           = "k8s.gcr.io/kubernetes-dashboard-amd64:v1.10.1"
      kube2iam                       = "jtblin/kube2iam:0.10.4"
      prometheus                     = "prom/prometheus:v2.19.0"
      alertmanager                   = "prom/alertmanager:v0.21.0"
      pushgateway                    = "prom/pushgateway:v1.2.0"
      kube_state_metrics             = "quay.io/coreos/kube-state-metrics:v1.9.7"
    }
    aws-cn = {
      helm_tiller                    = "${var.ecr_mirror}:helm-tiller-v2.16.1"
      cluster_autoscaler             = "${var.ecr_mirror}:cluster-autoscaler-v1.13.1"
      metrics_server                 = "${var.ecr_mirror}:metrics-server-v0.3.2"
      nginx_ingress_controller       = "${var.ecr_mirror}:0.25.0-nginx-ingress-controller" # Semver compatible
      default_backend                = "${var.ecr_mirror}:default-backend-1.5"
      cert_manager_controller        = "${var.ecr_mirror}:cert-manager-controller-v0.6.2"
      prometheus_cloudwatch_exporter = "${var.ecr_mirror}:prometheus-cloudwatch-exporter-0.8.0"
      kibana_oss                     = "${var.ecr_mirror}:kibana-oss-6.7.0"
      kubernetes_dashboard           = "${var.ecr_mirror}:kubernetes-dashboard-v1.10.1"
      kube2iam                       = "${var.ecr_mirror}:kube2iam-0.10.4"
      prometheus                     = "${var.ecr_mirror}:prometheus-9.2.0"
      alertmanager                   = "${var.ecr_mirror}:alertmanager-v0.21.0"
      pushgateway                    = "${var.ecr_mirror}:pushgateway-v1.2.0"
      kube_state_metrics             = "${var.ecr_mirror}:kube-state-metrics-v1.9.7"
    }
  }

  # Created by kubernetes TODO: make not statically pinned
  nginx_ingress_loadbalancer = {
    dev   = "internal-a50357ad6c49a11e9a76f067bdd25e55-607296889.eu-west-1.elb.amazonaws.com"
    qa    = "internal-a3f3ed93bb96711e990d306b8e5a6ba7-1049815172.eu-west-1.elb.amazonaws.com"
    stage = "internal-ab04a6c06da0a11e9b6e10243c3957de-2108414582.eu-west-1.elb.amazonaws.com"
    prod  = "internal-aa57857e9dfb311e99a7f06c4049894e-992177337.eu-west-1.elb.amazonaws.com"

    china-stage = "8.8.8.8"
  }

  ansible_directory = "${path.root}/artifacts/ansible/infra/10-${local.name}"

  ansible_tags = []

  ingress_ports        = concat(var.ingress_http_ports, [for row in var.ingress_tcp_ports : row.port])
  ingress_tcp_mappings = {for row in var.ingress_tcp_ports : row.port => row.service}

  ingress_dns = "ingress.${var.dns_suffix}"

  node_role_tag    = replace(format(var.shared_tag, "k8s-node-role"), "https://", "")
  node_role_system = "system"

  k8s_name = var.create ? element(split("*", join("*", [
    aws_eks_cluster.this[0].arn,
    aws_eks_cluster.this[0].name])), 1) : null

  k8s_owned_map = {
    format("kubernetes.io/cluster/%s", local.name) = "owned"
  }

  # https://docs.aws.amazon.com/eks/latest/userguide/add-user-role.html
  k8s_roles_mapping_nodes = {
    sg_id    = module.ig-shared-resources.ec2_nodes_sg_id
    role     = module.ig-shared-resources.iam_nodes_role_arn
    username = "system:node:{{EC2PrivateDNSName}}"
    groups   = [
      "system:bootstrappers",
      "system:nodes",
    ]
  }

  k8s_roles_mappings = concat([
    local.k8s_roles_mapping_nodes,
  ], var.api_sources)

  api_sources_sgs_ids = [for row in local.k8s_roles_mappings : row.sg_id if row.sg_id != null]

  system_srv_namespace       = "kube-system"
  system_srv_tolerations     = [
    {
      key      = local.node_role_tag
      operator = "Equal"
      value    = local.node_role_system
      effect   = "NoSchedule"
    },
    {
      key      = replace(format(var.shared_tag, "k8s-node-service"), "https://", "")
      operator = "Equal"
      value    = "k8s"
      effect   = "NoSchedule"
    },
    {
      key      = replace(format(var.shared_tag, "k8s-node-name"), "https://", "")
      operator = "Equal"
      value    = "k8s-system"
      effect   = "NoSchedule"
    },
  ]
  system_srv_tolerations_all = [
    {
      key      = local.node_role_tag
      operator = "Exists"
      effect   = "NoSchedule"
    },
    {
      key      = replace(format(var.shared_tag, "k8s-node-service"), "https://", "")
      operator = "Exists"
      effect   = "NoSchedule"
    },
    {
      key      = replace(format(var.shared_tag, "k8s-node-name"), "https://", "")
      operator = "Exists"
      effect   = "NoSchedule"
    },
    {
      key      = replace(format(var.shared_tag, "k8s-node-project"), "https://", "")
      operator = "Exists"
      effect   = "NoSchedule"
    },
  ]
  system_srv_node_selector   = map(
  local.node_role_tag,
  local.node_role_system,
  replace(format(var.shared_tag, "k8s-node-service"), "https://", ""),
  "k8s",
  replace(format(var.shared_tag, "k8s-node-name"), "https://", ""),
  "k8s-system",
  )
}
