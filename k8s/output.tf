output "eks_id" {
  value = element(concat(aws_eks_cluster.this.*.id, [
    ""]), 0)
}

output "eks_arn" {
  value = element(concat(aws_eks_cluster.this.*.arn, [
    ""]), 0)
}

output "eks_platform_version" {
  value = element(concat(aws_eks_cluster.this.*.platform_version, [
    ""]), 0)
}

output "ec2_ami_linux" {
  value = element(concat(data.aws_ami.eks_linux.*.image_id, [
    ""]), 0)
}

output "iam_nodes_role_name" {
  value = module.ig-shared-resources.iam_nodes_role_name
}

output "iam_nodes_role_arn" {
  value = module.ig-shared-resources.iam_nodes_role_arn
}

output "ec2_nodes_sg_id" {
  value = module.ig-shared-resources.ec2_nodes_sg_id
}

output "ec2_ami_windows" {
  value = element(concat(data.aws_ami.eks_windows.*.image_id, [
    ""]), 0)
}

output "k8s_cluster_name" {
  value = local.k8s_name
}

output "k8s_cert" {
  value = aws_eks_cluster.this.*.certificate_authority
}

output "k8s_endpoint" {
  value = element(concat(aws_eks_cluster.this.*.endpoint, [
    ""]), 0)
}

output "k8s_version" {
  value = element(concat(aws_eks_cluster.this.*.version, [
    ""]), 0)
}

output "lb_dns_name" {
  value = var.create ? aws_lb.ingress[0].dns_name : null
}

output "ingress_endpoint" {
  value = var.create ? local.ingress_dns : null

  depends_on = [
    aws_route53_record.ingress-lb,
  ]
}

output "addon_endpoints" {
  value = var.create ? {
    k8s-dashboard = {
      remote = aws_route53_record.addon-k8s-dashboard[0].name
      local  = aws_route53_record.addon-k8s-dashboard-local[0].name
    }
  } : null
}

output "istio_kiali_username" {
  value = module.istio.kiali_username
}

output "istio_kiali_password" {
  value = module.istio.kiali_password
}

output "k8s_disposable_namespace" {
  value = module.k8s-ns-disposable.k8s_namespace
}

output "k8s_disposable_pod_tolerations" {
  value = module.k8s-ns-disposable.k8s_pod_tolerations
}

output "k8s_disposable_pod_node_selector" {
  value = module.k8s-ns-disposable.k8s_pod_node_selector
}
