variable "create" {
  type    = bool
  default = true
}

locals {
  create_resources = var.create ? 1 : 0
}

variable "aws_region" {
  type = string
}

variable "ecr_mirror" {
  type = string
}

variable "aws_zones" {
  type = list(string)
}

variable "use_public_network" {
  type = bool
}

variable "k8s_version" {
  type = string
}

variable "ingress_http_ports" {
  type = list(number)

  default = [80, 443]
}

variable "ingress_tcp_ports" {
  type = map(object({
    port    = number
    service = string
  }))

  default = {}
}

variable "vpc_id" {
  type = string
}

variable "vpc_subnets" {
  type = list(string)
}

variable "vpc_subnets_cidrs" {
  type = list(string)
}

variable "key_name" {
  type = string
}

variable "iam_role" {
  type = string
}

variable "iam_boundary" {
  type = string
}

variable "module_prefix" {
  type = string
}

variable "shared_tag" {
  type = string
}

variable "infra_bucket" {
  type = string
}

variable "infra_prefix" {
  type = string
}

variable "environment" {
  type = string
}

variable "dns_zone" {
  type = string
}

variable "dns_suffix" {
  type = string
}

variable "lambda_stub_key" {
  type = string
}

variable "lambda_stub_bucket" {
  type = string
}

variable "env" {
  type = object({
    account_id         = string
    iam_eks_admin_role = string
  })
}

variable "api_sources" {
  type = list(object({
    sg_id    = string
    role     = string
    username = string
    groups   = list(string)
  }))

  default = []
}

variable "api_sg_count" {
  type    = number
  default = 0
}

variable "tags" {
  type    = map(string)
  default = {}
}

variable "name" {
  type    = string
  default = "k8s"
}

