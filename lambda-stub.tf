data "archive_file" "lambda-stub" {
  type        = "zip"
  source_dir  = "${path.module}/data/lambda-stub"
  output_path = "${path.root}/artifacts/lambda-stub.zip"
}

resource "aws_s3_bucket_object" "lambda-stub" {
  bucket  = aws_s3_bucket.infra.bucket
  key     = "${var.infra_prefix}lambda-stub.zip.base64"
  content = filebase64(data.archive_file.lambda-stub.output_path)
  etag    = data.archive_file.lambda-stub.output_md5

  lifecycle {
    ignore_changes = [
      etag,
      version_id,
    ]
  }

  tags         = local.tags
  content_type = "text/plain"
}

