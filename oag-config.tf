module "oag-config" {
  source = "./oag-config"

  infra_bucket = aws_s3_bucket.infra.bucket
  infra_prefix = var.infra_prefix
  create       = local.oag_available

  environment = var.environment
  tags = merge(local.tags, {
    format(local.shared_tag, "role") = "Shared resources"
  })
}

