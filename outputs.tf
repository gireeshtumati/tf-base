output "aws_region" {
  value = var.aws_region

  description = "AWS region for resources"
}

output "aws_zones" {
  value = local.vpc_zones

  description = "AWS zones for resources. DEPRECATED"
}

output "vpc_id" {
  value = local.vpc_id

  description = "Current environment VPC ID"
}

output "vpc_private_subnets" {
  value = local.vpc_private_subnets

  description = "Current environment VPC private subnets IDs"
}

output "vpc_public_subnets" {
  value = local.vpc_public_subnets

  description = "Current environment VPC public subnets IDs"
}

output "vpc_private_subnets_cidrs" {
  value = local.vpc_private_subnets_cidrs

  description = "Current environment VPC private subnets CIDRs"
}

output "vpc_public_subnets_cidrs" {
  value = local.vpc_public_subnets_cidrs

  description = "Current environment VPC public subnets CIDRs"
}

output "infra_bucket" {
  value = aws_s3_bucket.infra.bucket

  description = "Current environment private S3 bucket. For resources that don't depend on environment"
}

output "infra_public_bucket" {
  value = local.infra_public_bucket

  description = "Global infrastructure public S3 bucket. For resources that don't depend on environment"
}

output "env" {
  value = local.env

  description = <<EOF
    Current environment configuration map:
      account_id - AWS account id
      vpc_name - VPC name
      vpc_subnets_private - List of private VPC subnets names
      vpc_subnets_public - List of public VPC subnets names (empty for non-legacy)
      iam_permission_boundary - Name of IAM permission boundary policy
      is_legacy - True if environment is legacy (arch)
      is_basic - True if environment should be used for builds (dev)
      is_prod - True if customers use this environment
EOF
}

output "environment" {
  value = var.environment

  description = "Current environment"
}

output "account_id" {
  value = data.aws_caller_identity.current.account_id

  description = "Current environment AWS account id. DEPRECATED, use env.account_id"
}

output "permission_boundary" {
  value = local.permission_boundary

  description = "Current environment name of IAM permission boundary policy. DEPRECATED, use env.iam_permission_boundary"
}

output "shared_tag" {
  value = local.shared_tag

  description = "printf-like tag name pattern for AWSU tags"
}

output "tags" {
  value = local.tags

  description = "Map of tags for nested module resources"
}

output "tags_common" {
  value = local.tags_common

  description = "Common tags that should be used for all AWSU resources"
}

output "key_name" {
  value = aws_key_pair.user.key_name

  description = "Basic ssh key name. DEPRECATED, use per-resource ssh config"
}

output "ecr_proxy" {
  value = local.ecr_proxy

  description = "Global proxy repository docker url"
}

output "ecr_proxy_account_id" {
  value = local.ecr_proxy

  description = "Global proxy repository AWS account id"
}

output "email_host" {
  value = "email-smtp.${var.aws_region}.amazonaws.com"

  description = "SES host. DEPRECATED, use O365 SMTP server"
}

output "email_port" {
  value = "465"

  description = "SES port. DEPRECATED, use O365 SMTP server"
}

output "email_user" {
  value = local.email_user

  description = "SES user. DEPRECATED, use O365 SMTP server"
}

output "email_key" {
  value = local.email_key

  sensitive = true

  description = "SES password. DEPRECATED, use O365 SMTP server"
}

output "oag_available" {
  value = local.oag_available

  description = "True if OAG/AAG is used in current environment"
}

output "oag_endpoint_full" {
  value = local.oag_endpoint_full

  description = "printf-like OAG/AAG full service address"
}

output "oag_endpoint" {
  value = local.oag_endpoint

  description = "OAG/AAG hostname with protocol prefix"
}

output "oag_host" {
  value = local.oag_host

  description = "OAG/AAG hostname"
}

output "oag_templates_bucket" {
  value = module.oag-config.templates_bucket

  description = "OAG/AAG templates S3 bucket"
}

output "oag_templates_prefix" {
  value = module.oag-config.templates_prefix

  description = "OAG/AAG templates S3 bucket prefix"
}

output "is_basic_env" {
  value = local.is_basic_env

  description = "True if current environment is basic. DEPRECATED, use env.is_basic"
}

output "use_public_network" {
  value = local.use_public_network

  description = "True if current environment has public networks"
}

output "common_tags" {
  value = {}

  description = "DEPRECATED, use tags_common"
}

output "infra_tags" {
  value = {}

  description = "DEPRECATED, use tags_common"
}

output "tags_infra" {
  value = {}

  description = "DEPRECATED, use tags_common"
}

output "infra_prefix" {
  value = var.infra_prefix

  description = "S3 bucket key prefix for global resources"
}

output "service_variables_prefix" {
  value = "${var.infra_prefix}service-variables/"

  description = "S3 infra bucket prefix for service-dependent variable files"
}

output "module_prefix" {
  value = var.module_prefix

  description = "Common resource name prefix"
}

output "when_to_deploy" {
  value = {
    arch  = "on_success"
    dev   = "on_success"
    qa    = "manual"
    test  = "manual"
    stage = "manual"
    prod  = "manual"
  }
}

output "account_role_arn" {
  value = data.aws_caller_identity.current.arn
}

output "ami_aws_linux" {
  value = data.aws_ami.aws_linux.image_id
}

output "ami_eks_linux" {
  value = module.k8s.ec2_ami_linux
}

output "ami_eks_windows" {
  value = module.k8s.ec2_ami_windows
}

output "artifacts_directory" {
  value = "./artifacts"

  description = "Target directory for pipeline artifacts"
}

output "artifacts_ansible_directory" {
  value = "./ansible"

  description = "Target directory for pipeline ansible artifacts"
}

//output "ami_windows_2012" {
//  value = data.aws_ami.aws_windows_2012.image_id
//}
//
//output "ami_windows_2016" {
//  value = data.aws_ami.aws_windows_2016.image_id
//}

output "vpc_subnets" {
  value = local.vpc_subnets
}

output "vpc_subnets_cidrs" {
  value = local.vpc_subnets_cidrs
}

output "vpc_zones" {
  value = local.vpc_zones
}

output "dns_main_zone" {
  value = local.dns_main_zone
}

output "dns_main_suffix" {
  value = local.dns_main_suffix
}

output "dns_services_zone" {
  value = local.dns_services_zone
}

output "dns_services_suffix" {
  value = local.dns_services_suffix
}

output "lambda_stub_key" {
  value = aws_s3_bucket_object.lambda-stub.key

  description = "S3 bucket key with AWS lambda stub zip"
}

output "lambda_stub_bucket" {
  value = aws_s3_bucket_object.lambda-stub.bucket

  description = "S3 bucket with AWS lambda stub zip"
}

output "gitlab_runners" {
  value = module.gitlab-runner.runners
}

output "gitlab_runners_windows" {
  value = module.gitlab-runner-windows.runners
}

output "gitlab_runners_windows_passwords" {
  value = module.gitlab-runner-windows.windows_passwords
}

output "stub_ecr_repo" {
  value = aws_ecr_repository.stub.repository_url
}

output "eks_id" {
  value = module.k8s.eks_id
}

output "eks_arn" {
  value = module.k8s.eks_arn
}

output "eks_platform_version" {
  value = module.k8s.eks_platform_version
}

output "k8s_cluster_name" {
  value = module.k8s.k8s_cluster_name
}

output "k8s_cert" {
  value = module.k8s.k8s_cert
}

output "k8s_endpoint" {
  value = module.k8s.k8s_endpoint
}

output "k8s_version" {
  value = module.k8s.k8s_version
}

output "k8s_ingress_dns" {
  value = module.k8s.ingress_endpoint
}

output "addons_endpoints" {
  value = module.k8s.addon_endpoints
}

output "k8s_iam_nodes_role_name" {
  value = module.k8s.iam_nodes_role_name
}

output "k8s_iam_nodes_role_arn" {
  value = module.k8s.iam_nodes_role_arn
}

output "k8s_ec2_nodes_sg_id" {
  value = module.k8s.ec2_nodes_sg_id
}

output "route53_access_key" {
  value = var.route53_access_user

  sensitive = true
}

output "route53_secret_key" {
  value = var.route53_access_key

  sensitive = true
}

output "istio_kiali_username" {
  value = module.k8s.istio_kiali_username
}

output "istio_kiali_password" {
  value = module.k8s.istio_kiali_password
}

output "k8s_disposable_namespace" {
  value = module.k8s.k8s_disposable_namespace
}

output "k8s_disposable_pod_tolerations" {
  value = module.k8s.k8s_disposable_pod_tolerations
}

output "k8s_disposable_pod_node_selector" {
  value = module.k8s.k8s_disposable_pod_node_selector
}

output "certificate_services" {
  value = local.certificate_services
}