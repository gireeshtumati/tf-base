data "aws_caller_identity" "current" {
}

data "aws_partition" "current" {}

data "template_file" "prometheus-init-docker" {
  count    = local.create-instances
  template = file("${path.module}/data/init-docker.sh")

  vars = {
    REGION             = var.aws_region
    LOG_GROUP          = aws_cloudwatch_log_group.prometheus.name
    DISK_INODES        = var.prometheus_disk_inodes
    ENV                = upper(var.environment)
    ENV_LOWER          = lower(var.environment)
    CACHE_BUCKET       = var.prometheus_cache_bucket
    CACHE_PREFIX       = var.prometheus_cache_prefix
    PROMETHEUS_VERSION = var.prometheus_version
    GRAFANA_VERSION    = var.grafana_version
    PROMETHEUS_LIMIT   = var.prometheus_limit
    TAGS               = join(",", var.prometheus_tags)
  }
}

data "null_data_source" "prometheus-data" {
  count = var.prometheus_count

  inputs = {
    init-docker = <<EOF
#!/usr/bin/env bash
set -xe
export PROMETHEUS_ID=${count.index + 1}
${data.template_file.prometheus-init-docker[0].rendered}
EOF
}
}

locals {
name = "${var.module_prefix}prometheus${var.prometheus_type == "docker" ? "" : format("-%s", var.prometheus_type)}"
local-tags = {
Name = "${local.name}-${var.prometheus_type}"
}
tags = merge(var.tags, local.local-tags)
}

