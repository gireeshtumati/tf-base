#!/usr/bin/env bash
set -xe

mkdir -p /mnt/data
mount /dev/sdh /mnt/data || mkfs.ext4 -N ${DISK_INODES} /dev/sdh
mount /dev/sdh /mnt/data || true
touch /mnt/data/mounted

yum update -y
yum install -y docker
yum install -y git

curl -L https://github.com/docker/compose/releases/download/1.21.0/docker-compose-`uname -s`-`uname -m` | sudo tee /usr/local/bin/docker-compose > /dev/null
chmod +x /usr/local/bin/docker-compose
ln -s /usr/local/bin/docker-compose /usr/bin/docker-compose

echo "arch-poc.com ecdsa-sha2-nistp256 AAAAE2VjZHNhLXNoYTItbmlzdHAyNTYAAAAIbmlzdHAyNTYAAABBBMMn9MlbSyVzhkpxXgfPXipUQ2gd9qG0NftCuSo0p9cExQuR20/f/Zpuxs7nalmdqzoiE+QU1Or5679b6CZxCZw=" >> /etc/ssh/ssh_known_hosts
echo "gitlab.hilti.com,18.202.44.3 ecdsa-sha2-nistp256 AAAAE2VjZHNhLXNoYTItbmlzdHAyNTYAAAAIbmlzdHAyNTYAAABBBMMn9MlbSyVzhkpxXgfPXipUQ2gd9qG0NftCuSo0p9cExQuR20/f/Zpuxs7nalmdqzoiE+QU1Or5679b6CZxCZw=" >> /etc/ssh/ssh_known_hosts
echo "18.202.218.7 ecdsa-sha2-nistp256 AAAAE2VjZHNhLXNoYTItbmlzdHAyNTYAAAAIbmlzdHAyNTYAAABBBOzVRnJh2In1drDzflW42OxPuGkU7z2tWF4zCDfeE8j3H6wIXUdltPj0eWL2IUipqkBIXWL3DQYmCzm0XipyZ+0=" >> /etc/ssh/ssh_known_hosts

mkdir -p /root/.ssh
cat <<EOM > /root/.ssh/id_rsa.pub
ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAABAQCeneDqrmXua7BK2l7MrdLjyqYyeLvO7I6r5x5lGv2Qykf2KiGhNY8iFrGbvAShLvmEkOEXTCr7LeVhCl9Bp1bsbSqQbDtFiX+y2nX7dgzjtkHI8CIYoxhjY8bzgED7DUg6uH39q6ovuNcF7Nx/GZsa+s+NUK1UEm06uLePvcSeyoTtJ0ujv5RKltFjI11HBiWjMiJvU3iENRoyE3OiLn/DqWBPIRdVPbpVsyWIDMMLQKUQtJ5MxL7sfWmBh22ELRQnJu+gOrUL7rk8dgDeb1xcgQGXKGF0bzqFnLUsKo9JcHQnG2PPy4/+PCnWpRxsDl1QvC09Nm5GlDq0Pl2SW6bn
EOM

cat <<EOM > /root/.ssh/id_rsa
-----BEGIN RSA PRIVATE KEY-----
MIIEogIBAAKCAQEAnp3g6q5l7muwStpezK3S48qmMni7zuyOq+ceZRr9kMpH9ioh
oTWPIhaxm7wEoS75hJDhF0wq+y3lYQpfQadW7G0qkGw7RYl/stp1+3YM47ZByPAi
GKMYY2PG84BA+w1IOrh9/auqL7jXBezcfxmbGvrPjVCtVBJtOri3j73EnsqE7SdL
o7+USpbRYyNdRwYlozIib1N4hDUaMhNzoi5/w6lgTyEXVT26VbMliAzDC0ClELSe
TMS+7H1pgYdthC0UJybvoDq1C+65PHYA3m9cXIEBlyhhdG86hZy1LCqPSXB0Jxtj
z8uP/jwp1qUcbA5dULwtPTZuRpQ6tD5dklum5wIDAQABAoIBABO0rZK6Y/OA8X/7
LTfkiITAUnGGyl5kyTzWE9KfzczFNLeCeHccvlGP3DTq4PBdc6ZikXRhhB18kDjE
Hx16+rOYxGb460wyTukbdvjsq6FGrVmXa8T/zjHa3Ya28lu+RLD25sAzKYuQPTaB
PMuVlM1/R3XZBg+h0mwvMq7q9ph7gnwEfUz9a9zfrvYG56o2Rb2EGwQ61aJYhRGY
B+L4qFGifRtRN9/gzwYJ5XltdUf/3m4ucVOjRKOX08w72LxSZqe+uP4TGl2P601t
PEQyk2ZuDZKZgAVKbs5ZcgibT005/hbT76CRr2PiF1w2HrlLjLlEMvjV6bUaDNRz
BNIVTCECgYEAz+xOvzs/nT5BkoY1aPczVV33RJIXIMzHbljURJnHBPeUMzR1UHMm
FN71rkZoQED1AyUSQUMKOItPmyaRV2RcuUSOCaNDdEaVXnl+9xPTm88Je8M0d/u2
HWrkVQUECofUcpIqlJLKm562IMf7bIDsqVCM9rnplTVTgQdr/sA+7W8CgYEAw0r0
jZ6Opy0sMAgSb8fOyl2yVEONZ3aXIU/fvg/eyubooZIz8hzeBDP9z7E7aOYuAC6D
4VfPxVyzE3Zz4u56LGjPlYjhGqRzrPimnpelYtL4f1SUTyE8yhK5322Z2bIJUdvP
V02QVLmG0FOZjzy/Ca+J0i5A4gci243DDLkEkgkCgYAuWg3MD9dEpKZVldm+Rq2I
8GO5Q6AoTb5/2QmpKWb2soK1T5J/lsv8hx/Z9BQ1jsWQJ+mD5wAWTug2PEjWZKdP
HftUJrNY7otJ/mfRiPf5mhQRJRlDTjfxIu04DuAxwMoZqIAZkwfb3WvYrvxb2fno
XO6v6g4qlzLAZvkqV40vvQKBgDqgU8333Wc+XleE9NPtrZx+iDxXkymOJkxvyAEs
jntm2ks2srVqCRRF71ncILOa9hBD5n9NdygObgvXsW5cnPyb8kU3tCvlVnyEoLBX
YeTK5KKE54jht3gKKxZxto/trHKQhCv3Mu7cSyZbEmw3ljn4Im2gSQYRkltKsVUu
bbrRAoGANsAaNzmVBOxV5Qz0MQBplZ0nNEtBANPV3jLLOqYMg2Qhv9bUxa/yN/CW
AWHtn2kC6IZx186TDzBp9Ed3UXLqGOlBUjhaqGdNsnpdwZWsXaekVQTe8DuM6W5X
bjd92lBL3HxmHK/cldHs0BBE76Y1d3y8XwO9pBQIjCBItltI2/E=
-----END RSA PRIVATE KEY-----
EOM

chmod 400 /root/.ssh/id_rsa
chmod 400 /root/.ssh/id_rsa.pub

git clone --recurse-submodules  git@gitlab.hilti.com:infra/prometheus.git /mnt/data/prometheus
chmod 777 /mnt/data/prometheus


if [[ ! -d /mnt/data/docker ]]; then
  mv /var/lib/docker /mnt/data/docker
fi
cat > /etc/docker/daemon.json <<EOF
{
  "storage-driver": "overlay2",
  "data-root": "/mnt/data/docker",
  "log-driver": "awslogs",
  "log-opts": {
    "tag": "prometheus-$${PROMETHEUS_ID}.{{.Name}}.{{.ID}}",
    "awslogs-region": "${REGION}",
    "awslogs-group": "${LOG_GROUP}"
  }
}
EOF
service docker start
usermod -a -G docker ec2-user
git clone git@gitlab.hilti.com:infra/cloudwatch-exporter.git /mnt/data/cloudwatch-exporter
chmod 777 /mnt/data/cloudwatch-exporter

cd /mnt/data/cloudwatch-exporter
docker build -t cloudwatch-exporter:latest .

cd /mnt/data/prometheus

docker-compose up
