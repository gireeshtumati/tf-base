resource "aws_cloudwatch_log_group" "prometheus" {
  name              = "${local.name}/type-${var.prometheus_type}"
  retention_in_days = 7

  tags = local.tags
}

