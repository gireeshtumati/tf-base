output "terraform_role" {
  value = ""
}

output "prometheus" {
  value = aws_route53_record.main.*.name
}
