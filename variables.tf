variable "module_version" {
  type = string
}

variable "module_name" {
  type    = string
  default = "infra-base"
}

variable "environment" {
  type = string
}

variable "base_state_bucket" {
  type = string

  default = "hilti-infra"
}

variable "env_managed_map" {
  type    = map(string)
  default = {
    arch        = ""
    dev         = "dev"
    qa          = "qas"
    test        = "test"
    stage       = "stage"
    prod        = "prod"
    china-stage = "china-stage"
  }
}

variable "route53_access_user" {
  type = string
}

variable "route53_access_key" {
  type = string
}

variable "dns_managed_name" {
  default = "swu.hilti.cloud"
}

variable "infra_bucket" {
  type = string
}

variable "module_prefix" {
  type    = string
  default = "hilti-"
}

variable "aws_region" {
  type = string
}

variable "gitlab_runner_tags" {
  type    = list(string)
  default = [
    "aws",
    "type-docker"]
}

variable "infra_prefix" {
  default = "shared-artifacts/"
}

//variable "arch_assumed_role" {
//  type = string
//}

variable "user_key" {
  default = "ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAACAQC3FRxGJgBt2pE8aN4MULpLhEMZoBVHC10TaVFpHYDxTLpbVuVUAB/iNKilYjYpBvxtGo/Yv9mu/++i933kNEs7lSMqmhTdn6I1Ftel3UoFDnOAbrs3S/gVFns23bUrAvNcmJOGNjTxiU3VwmPbJwgPd75+K4rkp7VkfOp4eI1B0AJNK5WNwv6bmeVp5/eL+6EUaVI3R2bStWFTqIHVtmSW4oDWUNVbKb9TfnTaseUJsY997OtQ7oCmOvGiWOaGPEZ1cEyDg828uryeNuQ5DE4wY522PNk4l4JAd5Ft/fnHRfz0IHjVkrUqeQJXMmvWfLFLWjm4xH3y63EU6E2jRFFUkS1gA5yu5pBi6em7i8ScgaR4V6Jp8UGXXs+Qn/385aZlG6rT05TBXLe7lI700zxR5NVKKCqycky3gIjuYtomso8YDlwV8KPKo0OFFTR1Ys8WmrNTXVQaksiSC6gjHJpMZv1wLvJlFWt9QUcysJqCerpH6ZD1cMnz9hleGTvejPU/dYNpJH3nekEtVj+dFEjsK15TEVCQJuRwGaLwC2BJONf2ksT431FLzUulYitNloCtj6wqLOElf1AigvNK1CQDDQNmxF3ukGN9azcvcpba6n3K9Vdkc/WYoa+OdGN7C9UF24Yw+BUnzoJUOhlmsUzzjMf29jy4j7ozUVQ28oQNAQ=="
}
