data "aws_vpc" "main" {
  tags = {
    Name = local.env.vpc_name
  }
}

data "aws_subnet" "subnet_private" {
  count = length(local.env.vpc_subnets_private)
  tags  = {
    Name = local.env.vpc_subnets_private[count.index]
  }
}

data "aws_subnet" "subnet_public" {
  count = length(local.env.vpc_subnets_public)
  tags  = {
    Name = local.env.vpc_subnets_public[count.index]
  }
}


